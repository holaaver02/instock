<?php 
include 'connect.php';

$sql="Select * from `poster`";
$result=mysqli_query($con,$sql);
$row=mysqli_fetch_assoc($result);
$title=$row['title'];
$foto=$row['foto'];
$isi=$row['isi'];
$penjelasan=$row['penjelasan'];
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Stock</title>
</head>
	<style type="text/css">
		html,body{
			padding: 0;
			margin:0;
			font-family: sans-serif;
			background-color: #4F6367;
			color: white;
		}

		#instock{
			text-decoration: none;
			color: white;
		}

		h3 {
			padding-left: 30px;
		}

		h1 {
			padding-left: 15px;
			margin-top: -30px;
		}
		p {
			padding: 15px;
			width: 100%;
		}
		h2{
			font-weight: normal;
			font-size: 15px;
			text-align: center;
			color: #4F6367;
		}
		.merek{
			font-size: 25px;
			float: left;
			text-decoration: none;
			padding-left: 15px;
			padding-top: 0px;
			font-family: Times New Roman;
			font-weight: bold;
		}

		
		.menu-malasngoding{
			float: right;
			padding-top: 0px;
			margin-top: -10px;
		}
		.top {
			width: 100%;
			margin-top: -10px;
			background-color: #4F6367;

		}
		.menu-malasngoding ul {
			list-style-type: none;
			margin: 0;
			padding: 0px;
			overflow: hidden;
		}
	 
		.menu-malasngoding > ul > li {
			float: right;

		}
	 
		
		.menu-malasngoding li a {
			display: inline-block;
			color: white;
			text-align: center;
			padding: 14px 16px;
			text-decoration: none;
		}
	 
		.menu-malasngoding li a:hover{
			background-color: #fff;
			color: #4F6367 ;
		}
	 
		li.dropdown {
			display: inline-block;
		}
	 
		.dropdown:hover .isi-dropdown {
			display: block;

		}
	 
		.isi-dropdown a:hover {
			color: #fff !important;
		}
	 
		.isi-dropdown {
			position: absolute;
			display: none;
			box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
			z-index: 1;
			background-color: #f9f9f9;
		}
	 
		.isi-dropdown a {
			color: #3c3c3c !important;
		}
	 
		.isi-dropdown a:hover {
			color: #232323 !important;
			background: #f3f3f3 !important;
		}

		.menu-item a{
			color: white;
			text-decoration: none;
			text-align: left;
		}
		.poster img {
			width: 100%;
			height: 400px;
			object-fit: cover;
		}
		.poster {
			position: relative;
		}
		.gradien {
			background-image : linear-gradient(to bottom, rgba(255,255,255,0) , black);
			margin-top: 30px;
			height: 100px;
			width: 100%;
			position: absolute; bottom: 0;
		}
		.penjelasan {
			background-color: white;
			color: black;
			height: auto;
			width: 100%;
			padding-bottom: 1%;
		}

		.penjelasan p{
			line-height: 1.8;			
		}

	
		.k-penjelasan {
			width: 60%;
			float: right;
			margin-left: 0px;
			padding-left: 0px;
		}
		.keunggulan img{
			width: 40%;
			height: 280px;
			overflow: hidden;
			object-fit: cover;
		}
		.keunggulan{
			border: solid;
			border-color: grey;
			border-radius: 10px;
			border-width: 0.5px;
			margin-left: 1.5px;
			float: center;
			height: auto;
			padding: 10px;
		}

		.ajakan{
			float: left;
			width: 100%;
			background-color: white;
			margin-top: -3%;
			padding-top: 5%;
			
		}

		.coba_gratis{
			/*margin-left: 510px;*/
			float: left;
			background-color: white;
			width: 100%;
			padding-bottom: 5%;


		} 

		.kotak{
			margin-left: 42%;

		}
		.coba_gratis a {
			margin-left: 40px;
			width: 200px; 
			height: 35px; 
			font-size: 18px; 
			margin-top: 10px; 
			background-color: #7A9E9F; 
			padding: 10px; 
			color: #EEF5D5;
			text-decoration: none; 
			border-radius: 4px;
		}

		.coba_gratis a:hover{
			background-color: #4F6367;
			color: white;
		}

		.tengah {
			padding-left: 10px;
			width: 30%;
			float: left;
			height: 200px;
		}
		.kiri {
			width: 30%;
			float: left;
			height: 200px;
		}
		.kanan {
			width: 30%;
			height: 200px;
			float: right;
		}
		.col12 {
			width: 1500px;
		}
		.col1 {
			width: 60px;
		}
		.col2 {
			width: 140px;
		}
		.col3 {
			width: 220px;
		}
		.col4 {
			width: 300px;
		}
		.col5 {
		width: 380px;
		}
		.col6 {
		width: 460px;
		}
		.col7 {
		width: 540px;
		}
		.col8 {
		width: 620px;
		}
		.col9 {
		width: 700px;
		}
		.col10 {
		width: 780px;
		}
		.col11 {
		width: 860px;
		}
		.row {
			clear: both;
			margin: 10px 0;
		}
		.menu-item2 a{
			float: right;
			color: white;
			text-decoration: none;
		}

		.bawah h6 {
			font-size: 20px;
			text-decoration: none;
			padding-left: 15px;
			margin: 10px;
			font-family: Times New Roman;
			font-weight: bold;
		}
		.bawah p{
			margin: 10px;
			margin-top: -30px;
		}

		.bawah .tengah{
			margin-left: 80px;
		}

		.tengah #privasi{
			margin-top: -10px;
		}

		.tengah #privacypolicy{
			margin-top: -40px;
		}
		
		.tengah #privacypolicy a{
			margin-top: -100px;
		
		}

		.bawah a {
			color: white;
		}

		

	</style>

<body>
	<div class="container">
		<?php require('header.php');?>
		<br/>
		<br/>
		<div class="poster">
			<td valign="bottom"></td>
		 	<section class="gradien" >
				<h3><?php echo $title;?></h3>
				<h1><?php echo $isi;?></h1>
			</section>
				<img src="<?php echo $foto;?>">
		</div>
		<div class="penjelasan">
			
			<p><?php echo $penjelasan;?></p>
			
			<?php require('keunggulan.php');?>
			<br>
			<br>
		</div>

		<div class="ajakan">
				<h2>Tunggu apa lagi? Ayok kembangkan usaha Anda dengan layanan kami</h2>
			
			<br>
			<div class="coba_gratis">
				<div class="kotak">
					<a href= "registrasi.php"> Coba Gratis! </a>

				</div>
				
			</div>
		</div>
		
			<?php require('footer.php');?>

 	</div>
</body>

</html>