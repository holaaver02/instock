<?php 
include 'connect.php';

require('config.php');

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Stock</title>
</head>
	<style type="text/css">
		html,body{
			padding: 0;
			margin:0;
			font-family: sans-serif;
			background-color: #4F6367;
			color: white;
		}

		#instock{
			text-decoration: none;
			color: white;
		}

		.judul h3 {
			padding-left: 15px;
			font-size: 13px;
			float: left;
			padding-right: 20px;
		}
		.table a {
			float: right;
			margin-top: 30px;
			margin-right: 15px;
			color: grey;
			width: 30%;
		}
		h5 {
			padding-left: 135px;
			color: black;
		}

		h1 {
			padding-left: 15px;
		}
		p {
			padding: 15px;
			width: 100%;
		}
		h2{
			font-weight: normal;
			font-size: 15px;
			text-align: center;
			color: #4F6367;
		}
		.merek{
			font-size: 25px;
			float: left;
			text-decoration: none;
			padding-left: 15px;
			padding-top: 10px;
			font-family: Times New Roman;
			font-weight: bold;
		}

		
		.menu-malasngoding{
			float: right;
		}

		.menu-malasngoding ul {
			list-style-type: none;
			margin: 0;
			padding: 0;
			overflow: hidden;
		}
	 
		.menu-malasngoding > ul > li {
			float: right;
		}
	 
		
		.menu-malasngoding li a {
			display: inline-block;
			color: white;
			text-align: center;
			padding: 14px 16px;
			text-decoration: none;
		}
	 
		.menu-malasngoding li a:hover{
			background-color: #fff;
			color: #4F6367 ;
		}
	 
		li.dropdown {
			display: inline-block;
		}
	 
		.dropdown:hover .isi-dropdown {
			display: block;
		}
	 
		.isi-dropdown a:hover {
			color: #fff !important;
		}
	 
		.isi-dropdown {
			position: absolute;
			display: none;
			box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
			z-index: 1;
			background-color: #f9f9f9;
		}
	 
		.isi-dropdown a {
			color: #3c3c3c !important;
		}
	 
		.isi-dropdown a:hover {
			color: #232323 !important;
			background: #f3f3f3 !important;
		}

		.menu-item a{
			color: white;
			text-decoration: none;
			text-align: left;
		}

		.gradien {
			margin-top: 30px;
			width: 100%;
			position: absolute; bottom: 0;
		}

		.col12 {
			width: 100%;
		}
		.col1 {
			width: 8%;
		}
		.col2 {
			width: 145px;
		}
		.col3 {
			width: 220px;
		}
		.col4 {
			width: 270px;
		}
		.col5 {
		width: 380px;
		}
		.col6 {
		width: 460px;
		}
		.col7 {
		width: 56%;
		}
		.col8 {
		width: 620px;
		}
		.col9 {
		width: 700px;
		}
		.col10 {
		width: 780px;
		}
		.col11 {
		width: 860px;
		}
		.row {
			clear: both;
			margin: 10px 0;
		}
		.judul {
			float: left;
			color: grey;
			background-color: white;
		}
		.borderputih {
			float: left;
			width: 1480px;
			background: white;
			height: 880px;
			margin-left: 15px;
			border-radius: 10px;

		}

		.nilai h3 {
			color: black;
			font-size: 12px;
			margin-bottom: 0px;
		}
		img {
			width: 70px;
			height: 70px;
		}

		.gambar img{
			width: 100px;
			height: 100px;
			float: left;
			margin-right: 10px;
			overflow: hidden;
			object-fit: cover;
		}
	 
		.variandropdown:hover .isi-variandropdown {
			display: block;
			color: #3A3D43;
		}
	 
		.isi-variandropdown a:hover {
			color: #3A3D43 !important;
		}
	 
		.isi-variandropdown {
			position: absolute;
			display: none;
			box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
			z-index: 1;
			background-color: #f9f9f9;
		}
	 
		.isi-variandropdown a {
			color: #3A3D43 !important;
		}
	 
		.isi-variandropdown a:hover {
			color: #3A3D43 !important;
			background: #f3f3f3 !important;
		}
		.table {
			margin: 15px;
		}
		.table table{
			margin-top: 10px;
			background-color: white;
			border-radius: 10px;
			color: black;
			border: 1px solid;
		}
		.table h1 {
			float: left;
			font-size: 20px;
		}
		.table h3 {
			width: 100%;
			margin-top: 0px;
		}
		.table th {
			background-color: #f3f3f3;
			height: 30px;
		}
		.table a {
			margin-top: 20px;
			float: left;
			border: 1px solid;
			border-radius: 5px;
			text-align: center;
			background-color: gray;
			color: white;
			text-decoration: none;
			height: 20px;
			width: 100%;

		}

		#add_stock{
			margin-top: 20px;
			float: right;
			border-radius: 5px;
			text-align: center;
			color: #f3f3f3;
			text-decoration: none;
		}

		#add_stock a{
			text-decoration: none;
			color: #4F6367;
		}

		#add_stock a:hover{
			color: black;
		}


	</style>

<body>
	<div class="container">
		<div class="merek col1">
			INSTOCK
		</div>
		<div class="menu-malasngoding col7">
			<ul>
				<li class="menu-item"><a href="logout.php">Log out</a></li>
				<li class="dropdown"><a href="#">Bahasa</a>
					<ul class="isi-dropdown">
						<li><a href="#">Indonesia</a></li>
						<li><a href="#">English</a></li>
					</ul>
				</li>
				<li><a href="#">Bantuan</a>
				</li>

		 	</ul>

		</div>
		<br/>

		<section class=" judul col12">
			<section class="col12">
				<h3 style="color: black; margin-top: 15px; ">Daftar Produk</h3>
				<h3 style="margin-top: 15px"><a href="Transaksi.php" style="text-decoration: none; color: grey;">Transaksi</a></h3>
				<h3 style="margin-top: 15px; margin-right: 5px" id="add_stock"><a href="add_stock.php">Add Stock</a></h3>
			</section>
		</section>

		<div class="table">
			<h1 style="padding-top: 10px; font-weight: normal; margin-left: 50px;">Daftar Produk</h1>
			<input style="float: right; margin-top: 20px; margin-right: 65px;" type="text" name="Search" placeholder="Search" ;>
	
			<table style="width: 90%; margin: 0 auto;">
				<tr>
					<th>Produk</th>
					<th>Stock</th>
					<th>Edit</th>
				</tr>

		
					<?php
						$sql = "Select * from `stock`";
						$result=mysqli_query($con, $sql);
						if ($result) {
							
							while ($row=mysqli_fetch_assoc($result)) {
								$id=$row['id'];
								$gambar=$row['gambar'];
								$nama_produk=$row['nama_produk'];
								$small=$row['small'];
								$medium=$row['medium'];
								$large=$row['large'];
								echo '<tr>
								<td>
									<div class="gambar" style="margin-left: 10px;"">
										<img src="'.$gambar.'">
									</div>

									<div class="nilai">
										<h3 style="font-size: 15px; padding-right: 100px;">'.$nama_produk.'</h3>
										<br>
								</td>
								<td>
									<table style="font-size: 12px; width: 100%;">
										<tr>
											<td style="padding-right: 0px;">Small</td>
											<td>:</td>
											<td style="padding-left: 10px; margin-right:0px;">'.$small.'</td>
											<td style="margin-left:-10px;">pcs</td>
										</tr>
										<tr>
											<td>Medium</td>
											<td>:</td>
											<td style="padding-left: 10px;">'.$medium.'</td>
											<td>pcs</td>
										</tr>
										<tr>
											<td>Large</td>
											<td>:</td>
											<td style="padding-left: 10px;">'.$large.'</td>
											<td>pcs</td>
										</tr>
									</table>
									<a href="edit.php?editid='.$id.'" style="font-size: 14px; margin-top: 5px; margin-right: 0px; ">Edit Stock</a>
								</td>
								<td>
									<a href="update.php?updateid='.$id.'" style="margin-top: 0px;">Update</a>
									<a href="delete.php?deleteid='.$id.';" style="margin-top: 8px">Delete</a>
									
								</td>
							</tr>
					';
						}
					}else{
						die(mysqli_error($con));
					}
					?>
					
			</table>
		</div>
 	</div>
</body>

</html>