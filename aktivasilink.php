<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Pengaturan Stok</title>
</head>
	<style type="text/css">
		html,body{
			padding: 0;
			margin:0;
			font-family: sans-serif;
			background-color: #4F6367;
			color: white;
		}

		.container{
			width: 100%;
		}
		#instock{
			text-decoration: none;
			color: white;}

		.merek{
			font-size: 25px;
			float: left;
			text-decoration: none;
			padding-left: 15px;
			padding-top: 0px;
			font-family: Times New Roman;
			font-weight: bold;
		}

		
		.menu-malasngoding{
			float: right;
			padding-top: 0px;
			margin-top: -10px;
		}

		.row {
			clear: both;
			margin: 10px 0;
		}
		.top {
			width: 100%;
			margin-top: 0px;
			background-color: #4F6367;

		}
		.menu-malasngoding ul {
			list-style-type: none;
			margin: 0;
			padding: 0px;
			overflow: hidden;
		}
	 
		.menu-malasngoding > ul > li {
			float: right;

		}
	 
		
		.menu-malasngoding li a {
			display: inline-block;
			color: white;
			text-align: center;
			padding: 14px 16px;
			text-decoration: none;
		}
	 
		.menu-malasngoding li a:hover{
			background-color: #fff;
			color: #4F6367 ;
		}
	 
		li.dropdown {
			display: inline-block;
		}
	 
		.dropdown:hover .isi-dropdown {
			display: block;

		}
	 
		.isi-dropdown a:hover {
			color: #fff !important;
		}
	 
		.isi-dropdown {
			position: absolute;
			display: none;
			box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
			z-index: 1;
			background-color: #f9f9f9;
		}
	 
		.isi-dropdown a {
			color: #3c3c3c !important;
		}
	 
		.isi-dropdown a:hover {
			color: #232323 !important;
			background: #f3f3f3 !important;
		}

		.menu-item a{
			color: white;
			text-decoration: none;
			text-align: left;
		}

		.bawah h6 {
			font-size: 20px;
			text-decoration: none;
			padding-left: 15px;
			padding-top: 20px;
			margin: 10px;
			margin-bottom: -10px;
			font-family: Times New Roman;
			font-weight: bold;
		}
		.bawah p{
			margin: 10px;
			margin-top: 0px;
			margin-left: 22px;
		}

		.bawah .tengah{
			margin-left: 80px;
		}

		.tengah #privasi{
			margin-top: -10px;
		}

		.tengah #privacypolicy{
			margin-top: -40px;
		}
		
		.tengah #privacypolicy a{
			margin-top: -100px;
		
		}

		.bawah a {
			color: white;
		}

		.tengah {
			padding-left: 10px;
			width: 30%;
			float: left;
			height: 200px;
		}
		.kiri {
			width: 30%;
			float: left;
			height: 200px;
		}
		.kanan {
			width: 30%;
			height: 200px;
			float: right;
		}

		.baris {
			clear: both;
			margin: 10px 0;
		}

		.atas {
			width: 100%;
			margin-top: -10px;
			background-color: #4F6367;

		}

		.judul{
			font-size: 25px;
			float: left;
			text-decoration: none;
			padding-left: 15px;
			padding-top: 0px;
			font-family: Times New Roman;
			font-weight: bold;
			padding-top: 10px;
		}

		.col1 {
			width: 60px;
		}

		.col2 {
			width: 150px;
		}

		.col3{
			width: 400px;
		}

		.latarputih{
			width: 90%;
			height: 500px;
			background-color: white;
			border-radius: 10px;
			margin-top: -20px;
			float: left;
			margin-left: 5%;

		}

		.latarputih h1{
			padding-top: 20px;
			color: black;
			padding-left: 40px;
		}

		.gambar {
			margin-top: -100px;
			padding-right: 50px;
			float: right;

		}

		.gambar img{
			width: 500px;
		}

		h4{
			margin-left: 60px;

		}

	</style>
<body>
	<?php require('header.php');?>
	<div class="container">
		<div class="baris atas">
            <div class="judul col3">
                <h4>Aktivasi Link</h4>
             </div>
              	<div class="latarputih">
            		<h1>Temukan solusi akuntansi online yang tepat untuk bisnis Anda</h1>

            		<h5 style="color: grey; width: 500px; padding-left: 40px; padding-top: 20px; line-height: 20px; text-align: justify ;">Pengelolaan akuntansi akan bersinggungan dengan seluruh divisi, termasuk divisi HR. Pastikan pekerjaan akuntansi menggunakan data yang akurat, dan dapat dipercaya dengan bundle pengelolaan data keuangan karyawan dari Talenta Starter.</h5>
            		<div class="gambar">
            			<img src="pengaturanstok.jpg">	
            		</div>
            		
            	</div>
            </div>
            
        </div>
        
	</div>
	  <?php require('footer.php');?>

</body>
</html>