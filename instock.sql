-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2022 at 12:48 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `instock`
--

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

CREATE TABLE `footer` (
  `title` varchar(20) NOT NULL,
  `isi` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT current_timestamp(),
  `admin_name` varchar(10) NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp(),
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `footer`
--

INSERT INTO `footer` (`title`, `isi`, `status`, `admin_name`, `date`, `id`) VALUES
('Alamat', '<p>Jl XXXXXX No. 123</p>\r\n<p>Medan, Sumatera Utara</p>\r\n<p>&nbsp;</p>', 'Active', 'Admin', '2022-04-19', 7),
('Nomor Telepon', '<p>081234567801 - Saly</p>', 'Active', 'Admin1', '2022-04-19', 8),
('More', '<p>Privacy Policy</p>\r\n<p>About</p>\r\n<p>Help Center</p>', 'Active', 'admin', '2022-04-19', 9),
('Cabang', '<p>Medan- 081234567023</p>\r\n<p>&nbsp;</p>\r\n<p>Jakarta - 081234560981</p>\r\n<p>&gt;</p>', 'Inactive', 'amelia', '2022-04-19', 11);

-- --------------------------------------------------------

--
-- Table structure for table `keunggulan`
--

CREATE TABLE `keunggulan` (
  `title` varchar(20) NOT NULL,
  `isi` varchar(500) NOT NULL,
  `status` varchar(10) NOT NULL,
  `foto` varchar(20) NOT NULL,
  `id` int(10) NOT NULL,
  `last_edit` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `keunggulan`
--

INSERT INTO `keunggulan` (`title`, `isi`, `status`, `foto`, `id`, `last_edit`) VALUES
('Automasi', '<p>Layanan ini bertujuan untuk memudahkan para pelanggan dengan fitur automasinya. Pelanggan bisa menghubungkan semua aplikasi e-commerce yang digunakan dengan layanan ini sehingga penghitungan stock bisa langsung diatur secara otomatis.</p>', 'Active', 'keunggulan 1.jpg', 1, '2022-04-18'),
('Optimasi', '<p>Waktu yang terbuang sia-sia untuk mengsinkronisasi data stock setiap e-commerce bisa dihilangkan dengan layanan yang disediakan fitur kami.</p>', 'Inactive', 'keunggulan 2.jpg', 5, '2022-04-18');

-- --------------------------------------------------------

--
-- Table structure for table `poster`
--

CREATE TABLE `poster` (
  `title` varchar(100) NOT NULL,
  `foto` varchar(20) NOT NULL,
  `isi` varchar(1000) NOT NULL,
  `penjelasan` varchar(1000) NOT NULL,
  `admin_name` varchar(20) NOT NULL,
  `last_edit` date NOT NULL DEFAULT current_timestamp(),
  `id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `poster`
--

INSERT INTO `poster` (`title`, `foto`, `isi`, `penjelasan`, `admin_name`, `last_edit`, `id`) VALUES
('INSTOCK', 'poster.jpg', '<p>Aplikasi Stock Berbasis Web</p>', '<p>Aplikasi stok barang sederhana berbasis web untuk memantau persediaan barang dengan mudah. Dapatkan data jumlah stok, perhitungan persediaan, harga jual rata-rata dan notifikasi bila stok kosong secara langsung dan real time.</p>', 'admin', '2022-04-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `registrasi`
--

CREATE TABLE `registrasi` (
  `email` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `nomortelepon` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `registrasi`
--

INSERT INTO `registrasi` (`email`, `username`, `nomortelepon`, `password`) VALUES
('test@gmail.com', 'test', '0812345678901', 'test'),
('xxx@gmail.com', 'xxx', '081234567621', 'xxx'),
('felix@gmail.com', 'felix', '081234567802', 'felix');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(20) NOT NULL,
  `gambar` varchar(35) NOT NULL,
  `nama_produk` varchar(40) NOT NULL,
  `link` varchar(255) NOT NULL,
  `small` int(100) NOT NULL,
  `medium` int(100) NOT NULL,
  `large` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `gambar`, `nama_produk`, `link`, `small`, `medium`, `large`) VALUES
(9, '1.jpg', 'kemeja', '', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(10) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `small` int(10) NOT NULL,
  `medium` int(10) NOT NULL,
  `large` int(10) NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `nama_produk`, `small`, `medium`, `large`, `date`) VALUES
(1, 'kemeja', 1, 1, 1, '2022-04-19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `footer`
--
ALTER TABLE `footer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keunggulan`
--
ALTER TABLE `keunggulan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poster`
--
ALTER TABLE `poster`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `footer`
--
ALTER TABLE `footer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `keunggulan`
--
ALTER TABLE `keunggulan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `poster`
--
ALTER TABLE `poster`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
