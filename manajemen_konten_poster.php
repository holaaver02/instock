<?php 
include 'connect.php';

require('config.php');

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Stock</title>
</head>
	<style type="text/css">
		html,body{
			padding: 0;
			margin:0;
			font-family: sans-serif;
			background-color: #7A9E9F;
			color: white;
		}

		.judul h3 {
			padding-left: 15px;
			font-size: 13px;
			float: left;
			padding-right: 20px;
		}
		.table a {
			float: right;
			margin-top: 30px;
			margin-right: 15px;
			color: grey;
			width: 30%;
		}
		h5 {
			padding-left: 135px;
			color: black;
		}

		h1 {
			padding-left: 15px;
		}
		p {
			padding: 15px;
			width: 100%;
		}
		h2{
			font-weight: normal;
			font-size: 15px;
			text-align: center;
			color: #4F6367;
		}

		.container{
			background-color: #4F6367;
			height: 50px;

		}
		.merek{
			font-size: 25px;
			float: left;
			text-decoration: none;
			padding-left: 15px;
			padding-top: 10px;
			font-family: Times New Roman;
			font-weight: bold;
		}

		
		.menu-malasngoding{
			float: right;
		}

		.menu-malasngoding ul {
			list-style-type: none;
			margin: 0;
			padding: 0;
			overflow: hidden;
		}
	 
		.menu-malasngoding > ul > li {
			float: right;
		}
	 
		
		.menu-malasngoding li a {
			display: inline-block;
			color: white;
			text-align: center;
			padding: 14px 16px;
			text-decoration: none;
		}
	 
		.menu-malasngoding li a:hover{
			background-color: #fff;
			color: #4F6367 ;
		}
	 
		li.dropdown {
			display: inline-block;
		}
	 
		.dropdown:hover .isi-dropdown {
			display: block;
		}
	 
		.isi-dropdown a:hover {
			color: #fff !important;
		}
	 
		.isi-dropdown {
			position: absolute;
			display: none;
			box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
			z-index: 1;
			background-color: #f9f9f9;
		}
	 
		.isi-dropdown a {
			color: #3c3c3c !important;
		}
	 
		.isi-dropdown a:hover {
			color: #232323 !important;
			background: #f3f3f3 !important;
		}

		.menu-item a{
			color: white;
			text-decoration: none;
			text-align: left;
		}

		.gradien {
			margin-top: 30px;
			width: 100%;
			position: absolute; bottom: 0;
		}

		.col12 {
			width: 100%;
		}
		.col1 {
			width: 8%;
		}
		.col2 {
			width: 145px;
		}
		.col3 {
			width: 220px;
		}
		.col4 {
			width: 270px;
		}
		.col5 {
		width: 380px;
		}
		.col6 {
		width: 460px;
		}
		.col7 {
		width: 56%;
		}
		.col8 {
		width: 620px;
		}
		.col9 {
		width: 700px;
		}
		.col10 {
		width: 780px;
		}
		.col11 {
		width: 860px;
		}
		.row {
			clear: both;
			margin: 10px 0;
		}
		.judul {
			float: left;
			color: grey;
			background-color: white;
		}
		.borderputih {
			float: left;
			width: 1480px;
			background: white;
			height: 880px;
			margin-left: 15px;
			border-radius: 10px;

		}


		.nilai h3 {
			color: black;
			font-size: 12px;
			margin-bottom: 0px;
		}
		img {
			width: 70px;
			height: 70px;
		}

		.gambar img{
			width: 100px;
			height: 100px;
			float: left;
			margin-right: 10px;
			overflow: hidden;
			object-fit: cover;
		}
	 
		.variandropdown:hover .isi-variandropdown {
			display: block;
			color: #3A3D43;
		}
	 
		.isi-variandropdown a:hover {
			color: #3A3D43 !important;
		}
	 
		.isi-variandropdown {
			position: absolute;
			display: none;
			box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
			z-index: 1;
			background-color: #f9f9f9;
		}
	 
		.isi-variandropdown a {
			color: #3A3D43 !important;
		}
	 
		.isi-variandropdown a:hover {
			color: #3A3D43 !important;
			background: #f3f3f3 !important;
		}
		.table {
			float: left;
			margin-left: 45px;
			width: 80%;
		
		}
		.table table{
			margin-top: 10px;
			background-color: white;
			border-radius: 10px;
			color: black;
			border: 1px solid;
			padding: 5px;
		}
		.table h1 {
			float: left;
			font-size: 20px;
		}
		.table h3 {
			width: 100%;
			margin-top: 0px;
		}
		.table th {
			background-color: #f3f3f3;
			height: 30px;
		}
		.table a {
			margin-top: 20px;
			float: left;
			border: 1px solid;
			border-radius: 5px;
			text-align: center;
			background-color: gray;
			color: white;
			text-decoration: none;
			height: 20px;
			width: 100%;

		}

		.edit a{
			text-decoration: none;
			background-color: white;
		}

		.edit img{
			width: 25px;
			height: 25px;
			text-decoration: none;
			background-color: white;
			margin-top: -100px;
		}

		.menu_samping{
			float: left;
			background-color: #B8D8D8;
			height: 480px;
			color: black;
			
		}

		.menu_samping ul{
			margin-left: -40px;
		}

		.menu_samping li{
			list-style-type: none;

		}

		.menu_samping li a {
			display: inline-block;
			color: black;
			text-align: center;
			text-decoration: none;
		}

		.menu_samping li a:hover{
			background-color:#4F6367;
			color: white ;
		}

		.isi_menu_samping{
			padding: 20px;

		
		}

		.isi_menu_samping:hover{
			background-color: #4F6367;
		}

		.isi_menu_samping a{
			text-decoration: none;
			color: black;
		}

		.isi_menu_samping a:hover{
			color: white;
		}

		#add_entry{
			float: right;
			padding-right: 45px;
			margin-top: -25px;
		}

		#add_entry a{
			background-color: #EEF5DB;
			padding: 5px;
			color: black;
		}

		#add_entry a:hover{
			background-color: #4F6367;
			color: white;
		}

		.active{
			color: green;
		}

	</style>

<body>
	<div class="container">
		<div class="merek col1">
			INSTOCK
		</div>
		<div class="menu-malasngoding col7">
			<ul>
				<li class="menu-item"><a href="logout.php">Log out</a></li>
				<li class="dropdown"><a href="#">Bahasa</a>
					<ul class="isi-dropdown">
						<li><a href="#">Indonesia</a></li>
						<li><a href="#">English</a></li>
					</ul>
				</li>
				<li><a href="#">Bantuan</a></li>
		 	</ul>

		</div>

		<br>

		<section class=" judul col12">
			<section class="col12">
				<h3 style="color: black; margin-left: 5px;">Administrator</h3>
			</section>

		</section>


		<div class="menu_samping">
			<ul>
				<li class="isi_menu_samping" style="margin-top: -16px; border-top: 1px black solid;"><a href="manajemen_konten_poster.php">Poster dan Tulisan</a></li>
				<li class="isi_menu_samping"><a href="manajemen_konten_keunggulan.php">Keunggulan</a></li>
				<li class="isi_menu_samping"><a href="manajemen_konten_footer.php">Footer</a></li>
		 	</ul>
			
			
		</div>

		<div class="table">
			<h1 style="padding-top: 10px; font-weight: normal;">Poster dan Tulisan</h1>
			
			<table style="width: 100%;">
				<tr>
					<td colspan="3">
						<input style=" width: 80%; margin: 10px;  margin-bottom: 15px;" type="text" name="Search" placeholder="Search">
					</td>

					<td colspan="3">
						<div id="add_entry">
						</div>
					</td>
				
				</tr>
				<tr>
					<th>Title</th>
					<th>foto</th>
					<th>Last edit</th>
					<th>Edit</th>
				</tr>
				
				<?php
						$sql = "Select * from `poster`";


						$result=mysqli_query($con, $sql);
						if ($result) {
							
							while ($row=mysqli_fetch_assoc($result)) {
								$title=$row['title'];
								$foto=$row['foto'];
								$last_edit=$row['last_edit'];	
								$id=$row['id'];			
								echo '
								<tr>
					<td>
						'.$title.'
					</td>

					<td>
						<img src="'.$foto.'">
					</td>

					<td>
						'.$last_edit.'
					</td>

					<td class ="edit">
						<a href="edit_poster.php?editid='.$id.'"><img src="edit.png"></a>
					</td>

				</tr>
					';
						}
					}else{
						die(mysqli_error($con));
					}
					?>
				
					
					
			</table>
		</div>
 	</div>
</body>

</html>