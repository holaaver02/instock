<?php 
include 'connect.php';

require('config.php');
if (isset ($_POST['submit'])){
	$title=$_POST['title'];
	$admin_name=$_POST['admin_name'];
	$status=$_POST['status'];
	$isi=$_POST['isi'];

	$sql="insert into `footer` (title, admin_name, status, isi) values('$title', '$admin_name', '$status', '$isi')";

	$result = mysqli_query($con,$sql);

	if ($result) {
		echo "Data inserted successfully";
	}else{
		die(mysqli_error($con));
	}

	header('location:manajemen_konten_footer.php');

}

?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Stock</title>

	<script src='tinymce/tinymce.min.js'></script>

	<script>
		tinymce.init({
    		selector: '#isi',
    		plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
    		imagetools_cors_hosts: ['picsum.photos'],
    		menubar: 'file edit view insert format tools table help',
    		toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
		    toolbar_sticky: false,
		    autosave_ask_before_unload: true,
		    autosave_interval: "30s",
		    autosave_prefix: "{path}{query}-{id}-",
		    autosave_restore_when_empty: false,
		    autosave_retention: "2m",
		    image_advtab: true,
		    /*content_css: '//www.tiny.cloud/css/codepen.min.css',*/
		    link_list: [
		        { title: 'My page 1', value: 'https://www.codexworld.com' },
		        { title: 'My page 2', value: 'https://www.xwebtools.com' }
		    ],
		    image_list: [
		        { title: 'My page 1', value: 'https://www.codexworld.com' },
		        { title: 'My page 2', value: 'https://www.xwebtools.com' }
		    ],
		    image_class_list: [
		        { title: 'None', value: '' },
		        { title: 'Some class', value: 'class-name' }
		    ],
		    importcss_append: true,
		    file_picker_callback: function (callback, value, meta) {
		        /* Provide file and text for the link dialog */
		        if (meta.filetype === 'file') {
		            callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
		        }
		    
		        /* Provide image and alt text for the image dialog */
		        if (meta.filetype === 'image') {
		            callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
		        }
		    
		        /* Provide alternative source and posted for the media dialog */
		        if (meta.filetype === 'media') {
		            callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
		        }
		    },
		    templates: [
		        { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
		        { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
		        { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
		    ],
		    template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
		    template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
		    height: 300,
		    width: 810,

		    image_caption: true,
		    quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
		    noneditable_noneditable_class: "mceNonEditable",
		    toolbar_mode: 'sliding',
		    contextmenu: "link image imagetools table",
		});

	</script>

</head>
	<style type="text/css">
		html,body{
			padding: 0;
			margin:0;
			font-family: sans-serif;
			background-color: #7A9E9F;
			color: white;
		}

		#instock{
			text-decoration: none;
			color: white;
		}

		.judul h3 {
			padding-left: 15px;
			font-size: 13px;
			float: left;
			padding-right: 20px;
		}
		.table a {
			float: right;
			margin-top: 30px;
			margin-right: 15px;
			color: grey;
			width: 30%;
		}
		h5 {
			padding-left: 135px;
			color: black;
		}

		h1 {
			padding-left: 15px;
		}
		p {
			padding: 15px;
			width: 100%;
		}
		h2{
			font-weight: normal;
			font-size: 15px;
			text-align: center;
			color: #4F6367;
		}

		.container{
			background-color: #4F6367;
			height: 50px;

		}
		.merek{
			font-size: 25px;
			float: left;
			text-decoration: none;
			padding-left: 15px;
			padding-top: 10px;
			font-family: Times New Roman;
			font-weight: bold;
		}

		
		.menu-malasngoding{
			float: right;
		}

		.menu-malasngoding ul {
			list-style-type: none;
			margin: 0;
			padding: 0;
			overflow: hidden;
		}
	 
		.menu-malasngoding > ul > li {
			float: right;
		}
	 
		
		.menu-malasngoding li a {
			display: inline-block;
			color: white;
			text-align: center;
			padding: 14px 16px;
			text-decoration: none;
		}
	 
		.menu-malasngoding li a:hover{
			background-color: #fff;
			color: #4F6367 ;
		}
	 
		li.dropdown {
			display: inline-block;
		}
	 
		.dropdown:hover .isi-dropdown {
			display: block;
		}
	 
		.isi-dropdown a:hover {
			color: #fff !important;
		}
	 
		.isi-dropdown {
			position: absolute;
			display: none;
			box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
			z-index: 1;
			background-color: #f9f9f9;
		}
	 
		.isi-dropdown a {
			color: #3c3c3c !important;
		}
	 
		.isi-dropdown a:hover {
			color: #232323 !important;
			background: #f3f3f3 !important;
		}

		.menu-item a{
			color: white;
			text-decoration: none;
			text-align: left;
		}

		.gradien {
			margin-top: 30px;
			width: 100%;
			position: absolute; bottom: 0;
		}

		.col12 {
			width: 100%;
		}
		.col1 {
			width: 8%;
		}
		.col2 {
			width: 145px;
		}
		.col3 {
			width: 220px;
		}
		.col4 {
			width: 270px;
		}
		.col5 {
		width: 380px;
		}
		.col6 {
		width: 460px;
		}
		.col7 {
		width: 56%;
		}
		.col8 {
		width: 620px;
		}
		.col9 {
		width: 700px;
		}
		.col10 {
		width: 780px;
		}
		.col11 {
		width: 860px;
		}
		.row {
			clear: both;
			margin: 10px 0;
		}
		.judul {
			float: left;
			color: grey;
			background-color: white;
		}
		.borderputih {
			float: left;
			width: 1480px;
			background: white;
			height: 880px;
			margin-left: 15px;
			border-radius: 10px;

		}


		.nilai h3 {
			color: black;
			font-size: 12px;
			margin-bottom: 0px;
		}
		img {
			width: 70px;
			height: 70px;
		}

		.gambar img{
			width: 100px;
			height: 100px;
			float: left;
			margin-right: 10px;
			overflow: hidden;
			object-fit: cover;
		}
	 
		.variandropdown:hover .isi-variandropdown {
			display: block;
			color: #3A3D43;
		}
	 
		.isi-variandropdown a:hover {
			color: #3A3D43 !important;
		}
	 
		.isi-variandropdown {
			position: absolute;
			display: none;
			box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
			z-index: 1;
			background-color: #f9f9f9;
		}
	 
		.isi-variandropdown a {
			color: #3A3D43 !important;
		}
	 
		.isi-variandropdown a:hover {
			color: #3A3D43 !important;
			background: #f3f3f3 !important;
		}
		.table {
			float: left;
			width: 90%;
			margin-left: 4%;
		
		}
		.table table{
			margin-top: 10px;
			margin-left: 15px;
			background-color: white;
			border-radius: 10px;
			color: black;
			border: 1px solid;
			padding: 5px;
		}
		.table h1 {
			float: left;
			font-size: 20px;
		}
		.table h3 {
			width: 100%;
			margin-top: 0px;
		}
		
		.table a {
			margin-top: 20px;
			float: left;
			border: 1px solid;
			border-radius: 5px;
			text-align: center;
			background-color: gray;
			color: white;
			text-decoration: none;
			height: 20px;
			width: 100%;

		}

		table input{
			width: 90%;
			height: 25px;
			font-size: 15px;
			padding-left: 10px;

		}

		.isi{
			margin: 100px;
		}

		.table label{
			margin-left: 20px;
			margin-right: 20px;
			font-size: 15px;

		}
		td {
  			padding-top: 30px;
  			padding-left: 50px;
		}

		select{
			width: 91.5%;
			height: 25px;
			font-size: 15px;
			padding-left: 10px;

		}


	


	</style>

<body>
	<div class="container">
		<div class="merek col1">
			INSTOCK
		</div>
		<div class="menu-malasngoding col7">
			<ul>
				<li class="menu-item"><a href="logout.php">Log out</a></li>
				<li class="dropdown"><a href="#">Bahasa</a>
					<ul class="isi-dropdown">
						<li><a href="#">Indonesia</a></li>
						<li><a href="#">English</a></li>
					</ul>
				</li>
				<li><a href="#">Bantuan</a></li>
		 	</ul>

		</div>

		<br>

		<section class=" judul col12">
			<section class="col12">
				<h3 style="color: black; margin-left: 5px;">Administrator</h3>
			</section>

		</section>



		<div class="table">
			<h1 style="padding-top: 10px; font-weight: normal; padding-left:25px;">Add Entry</h1>
			
				<table style="width: 100%;">
					<form method="post">
					<tr >
						<td>
						<label for="title">Title</label>	
						</td>

						<td>
							<input type="text" name="title">
						</td>
					
					</tr>

					<tr>
						
						<td>
							<label for="isi">Isi</label>
						</td>

						<td>
							<textarea name="isi" id="isi"></textarea> 
						</td>
					
					</tr>

					<tr>
						
						<td>
							<label for="owner">Admin Name</label>
						</td>

						<td>
							<input type="text" name="admin_name">
						</td>
					
					</tr>

					<tr>
						
						<td>
							<label for="status">Status</label>
						</td>

						<td>
							<select id="status" name="status" style="height:25px;">
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</td>
					
					</tr>

					<tr>
						<td colspan="2"><input type="submit" name="submit" value="Save" style="width: 408px; height: 35px; font-weight: bold; font-size: 15px; margin: 20px; margin-left: 350px;"></td>
					</tr>
					</form>
						
				</table>	
			
		</div>
 	</div>
</body>

</html>