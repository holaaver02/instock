<?php 
include 'connect.php';

require('config.php');
if (isset ($_POST['submit'])){
	$email=$_POST['email'];
	$username=$_POST['username'];
	$nomortelepon=$_POST['nomortelepon'];
	$password=$_POST['password'];

	$sql = "insert into `registrasi`(email, username, nomortelepon, password) values('$email', '$username', '$nomortelepon', '$password')";

	$result = mysqli_query($con,$sql);

	if ($result) {
		echo "Data inserted successfully";
	}else{
		die(mysqli_error($con));
	}
	
header('location:stock.php');

}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Registrasi Akun</title>
	<style type="text/css">

		body{
			padding: 0;
			margin:0;
			font-family: sans-serif;
			background-image: url(pattern.jfif);
			color: white;
		}
		
		#isi{
			margin:  0 auto;
			background-color: #4F6367;
			width: 500px;
			height: 525px;
			border-radius: 7px;
		}

		h1{
			
			padding-top: 20px;
			font-family: Times New Roman;
			text-align: center;
		}

		h3{
			margin-left: 40px;
		}

		form{
			margin-left: 40px;
		}

		input{
			margin-top: 10px;
			margin-bottom: 10px;
			height: 20px;

		}

		input:hover{
			box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
		}

		label{
			font-size: 14px;
		}

		.register input:hover{
			color: #4F6367;
			background-color: white;
		}

		#punya_akun{
			text-align: right;
			margin-right: 55px;
			margin-top: 0px;
			font-size: 12px;
		}

		#punya_akun a{
			text-decoration: none;
			color: #FE5F55;
		}

		#punya_akun a:hover{
			color: #7A9E9F;
		}

	</style>
</head>

<body>

	<div id="isi">	
	
		<h1>INSTOCK</h1>

		<h3>Pendaftaran Akun Baru</h3>

		<form method="post">
			
			<label for="email">Email</label>
			<br>
			<input type="text" name="email" id="email" style="width: 400px;" autocomplete="off">
			<br>
			<label for="username">Username</label>
			<br>
			<input type="text" name="username" id="username" style="width: 400px;" autocomplete="off">
			<br>
			<label for="">Nomor Telepon</label>
			<br>
			<input type="tel" name="nomortelepon" id="nomortelepon" style="width : 400px;" autocomplete="off">
			<br>
			<label for="password">Password</label>
			<label for="konfirmasi_password" style="margin-left: 155px;">Konfirmasi Password</label>
			<br>
			<input type="password" name="password" id="password" style="width: 180px;" autocomplete="off">
			
			<input type="password" name="konfirmasi_password" id="konfirmasi_password" style="margin-left: 28px; width: 180px;" autocomplete="off">

			<p style="font-size: 10px; margin-top: -5px;">Gunakan kombinasi huruf dan angka serta minimal 1 karakter khusus.</p>

			<br>
			<div class="register">
				<input type="submit" name="submit" value="DAFTAR" style="width: 408px; height: 35px; font-weight: bold; font-size: 15px;">
				<p id="punya_akun">Sudah punya akun? <a href="login.php">Login</a></p>


			</div>
			

			

		</div>




	</form>

</body>
</html>