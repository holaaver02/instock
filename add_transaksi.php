<?php 
include 'connect.php';

require('config.php');
if (isset ($_POST['submit'])){
	$id=$_POST['id'];
	$sql="Select * from `stock` where id=$id";
	$result=mysqli_query($con,$sql);
	$row=mysqli_fetch_assoc($result);
	$id=$row['id'];
	$nama_produk=$row['nama_produk'];
	$small=$_POST['small'];
	$medium=$_POST['medium'];
	$large=$_POST['large'];

	$sql="insert into `transaksi` (nama_produk, small, medium, large) values('$nama_produk', '$small', '$medium', '$large')";
	$result = mysqli_query($con,$sql);
	if ($result) {
		echo "Data inserted successfully";
	}else{
		die(mysqli_error($con));
	}
	$small=$row['small'] - $small;
	$medium=$row['medium'] - $medium;
	$large=$row['large'] - $large;
	$sql="update stock set id = $id, small = '$small', medium = '$medium', large ='$large' where id=$id";

	$result = mysqli_query($con,$sql);


	if ($result) {
		echo "Data inserted successfully";
	}else{
		die(mysqli_error($con));
	}

	header('location:Transaksi.php');

}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Instock - Add Stock</title>
	<style type="text/css">

		body{
			padding: 0;
			margin:0;
			font-family: sans-serif;
			background-image: url(pattern.jfif);
			color: white;
		}
		
		#isi{
			margin: 0 auto;
			margin-top: 100px;
			background-color: #4F6367;
			width: 490px;
			padding-bottom: 25px;
			border-radius: 7px;
		}

		h1{
			
			padding-top: 20px;
			font-family: Times New Roman;
			text-align: center;
		}

		h3{
			margin-left: 40px;
		}

		form{
			margin-left: 40px;
		}

		input{
			margin-top: 10px;
			margin-bottom: 10px;
			height: 20px;

		}

		input:hover{
			box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
		}

		label{
			font-size: 14px;
		}

		.submit input:hover{
			color: #4F6367;
			background-color: white;
		}

		#punya_akun{
			text-align: right;
			margin-right: 55px;
			margin-top: 0px;
			font-size: 12px;
		}

		#punya_akun a{
			text-decoration: none;
			color: white;
		}

		#punya_akun a:hover{
			color: #7A9E9F;
		}

	</style>
</head>

<body>

	<div id="isi">	
	
		<h1>INSTOCK</h1>

		<h3>Tambah Produk</h3>

		<form method="post">
			
			<label for="nama_produk">Nama Produk</label>
			<br>
			<select name="id" style="width: 400px;">
				<?php 
				$sql = "Select * from `stock`";
						$result=mysqli_query($con, $sql);
						if ($result) {
							
							while ($row=mysqli_fetch_assoc($result)) {
								$nama_produk=$row['nama_produk'];
								$id=$row['id'];
								echo '<option value='.$id.'>'.$nama_produk.'</option>';
							}
						}
				?>
			</select>
			<br>
			<label for="small">Small</label>
			<input type="text" name="small" style="width: 80px; background-color: white;">
			<label for="">Medium</label>
			<input type="text" name="medium" style="width : 80px;">
			<label for="password">Large</label>
			<input type="text" name="large" style="width : 80px;">

			<br>
			<div class="submit">
				<input type="submit" name="submit" value="Tambah Produk" style="width: 408px; height: 35px; font-weight: bold; font-size: 15px; margin-top: 20px;">

			</div>
		</form>
	</div>

</body>
</html>